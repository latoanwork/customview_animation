package com.toanlt4.mockcustomview

object Constant {
    //Offset from dial radius to draw text label
    const val RADIUS_OFFSET_LABEL = 100
     const val START_ANGLE_ARC = 150F
     const val SWEEP_ANGLE_ARC = 240F


     const val MARGIN_LEFT_PARENT = 0.08f
     const val MARGIN_TOP_PARENT = 0.05f
     const val MARGIN_RIGHT_PARENT = 0.03f
     const val MARGIN_BOTTOM_PARENT = 0.05f

     const val DEFAULT_MAX_VALUE = 10
     const val DEFAULT_MIN_VALUE = -30
     const val STEP = 10
     const val MARGIN_TEXT_END = 15F


}