package com.toanlt4.mockcustomview

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    private var bottomView: BottomView? = null
    private var btnStart: Button? = null
    private var chartView: ChartView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initUI()
        initData()
        initAction()
    }

    private fun initAction() {
        btnStart?.setOnClickListener {
            bottomView?.startAnim()
        }
    }

    private fun initData() {
        val data =
            floatArrayOf(9F, -3F, 8F, -15F, -6F, -11F, -4F, -12F, -6F, -11F, 6F, -6F, 4F, -13F, 11F)
        chartView?.setData(data)
    }

    private fun initUI() {
        chartView = findViewById(R.id.viewChart)
        btnStart = findViewById(R.id.btnStart)
        bottomView = findViewById(R.id.viewBottom)
    }

}